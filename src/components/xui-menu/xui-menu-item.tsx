import { Component, Element, Prop, Method, Listen, Event, EventEmitter, PropDidChange } from '@stencil/core';

@Component({
  tag: 'xui-menu-item',
  styleUrl: 'xui-menu.scss'
})
export class MenuItem {
  @Element() el: HTMLElement;

  @Prop() active: boolean = false;
  @Prop() dataTab: string;

  @Event() itemActivated: EventEmitter;

  @Listen('click') onClick() { this.activate(); }
  @Listen('keydown.enter') onKeyEnter() { this.activate(); }
  @Listen('keydown.space') onKeySpace() { this.activate(); }

  activate() { this.itemActivated.emit(this.el); }

  componentDidLoad() {
    this.el.classList.add('item');
    this.updateActive(this.active);
    if (!!this.el.tabIndex) { this.el.tabIndex = 0; }
  }

  @PropDidChange('active')
  updateActive(active: boolean) {
    const update = this.active ? 'add' : 'remove';
    this.el.classList[update]('active');
  }

  render() {
    return <slot></slot>;
  }
}
