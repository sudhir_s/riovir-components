import { Component, Element, Prop, Method, Event, EventEmitter, PropDidChange } from '@stencil/core';

@Component({
  tag: 'xui-accordion-title'
})
export class AccordionTitle {
  @Element() el: HTMLElement;

  @Prop() active: boolean = false;

  @Event() panelToggled: EventEmitter;

  componentDidLoad() {
    this.el.classList.add('title');
    this.updateActive(this.active);
  }

  @PropDidChange('active')
  updateActive(active: boolean) {
    const update = this.active ? 'add' : 'remove';
    this.el.classList[update]('active');
  }

  render() {
    return <div class={{ title: true, active: this.active }} tabindex="0" onClick={() => this.panelToggled.emit(this.el)}>
      <i class="dropdown icon"></i><slot></slot>
    </div>;
  }
}
