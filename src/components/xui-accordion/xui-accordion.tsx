import { Component, Element, Listen } from '@stencil/core';

@Component({
  tag: 'xui-accordion'
})
export class Accordion {
  @Element() el;

  @Listen('panelToggled')
  onPanelToggled(event) {
    const titleEl = event.detail;
    titleEl.active = !titleEl.active;
    const panelPairs = this.el.children[0].children;

    const titleIndex = Array.prototype.slice
      .call(panelPairs)
      .indexOf(titleEl);

    panelPairs[titleIndex + 1].active = titleEl.active;
  }

  render() {
    return <div class="ui accordion">
      <slot></slot>
    </div>;
  }
}
