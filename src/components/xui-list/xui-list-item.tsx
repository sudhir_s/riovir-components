import { Component, Element, Prop, PropDidChange } from '@stencil/core';


@Component({
  tag: 'xui-list-item'
})
export class MyName {
  @Element() el: HTMLElement;

  @Prop() icon: string;
  @Prop() header: string;
  @Prop() description: string;

  componentDidLoad() {
    this.el.classList.add('item');
  }

  render() {
    return [
      <i class={`large ${this.icon} middle aligned icon`}></i>,
      <div class="content">
        <a class="header">
          {this.header ? (this.header) : (<slot name="header" />)}
        </a>
        <div class="description">
          {this.description ? (this.description) : (<slot />)}
        </div>
      </div>];
  }
}
