import { Component, Element } from '@stencil/core';

@Component({
  tag: 'xui-container'
})
export class Container {
  @Element() el: HTMLElement;
  componentDidLoad() { this.el.classList.add('ui', 'container'); }
}
